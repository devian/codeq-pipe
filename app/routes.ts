import * as Router from 'koa-router';

const router = new Router();

/**
 * Base route, return a 401
 */
router.get('/', async ctx => ctx.status = 200);

router.get('/moo', async ctx => {
    ctx.body = 'moo does the cow'
    ctx.status = 200
})

/**
 * Basic healthcheck
 */
router.get('/healthcheck', async ctx => ctx.body = 'OK');

export const routes = router.routes();
